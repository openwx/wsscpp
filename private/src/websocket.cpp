#include "websocket.h"

WebSocket::WebSocket(int timeout) : timeoutGlob(timeout)
{
    datGlobal = "";
    closed = false;
}

WebSocket::WebSocket()
{
    timeoutGlob = 30;
    datGlobal = "";
    closed = false;
}

WebSocket::~WebSocket()
{
    if (q.valid())
    {
        q.wait();
    }
    
    // work.reset();
    // ioc.stop();
    // clearSocket();
}

void WebSocket::startSocket(std::string host, std::string port, std::string text)
{
    q = std::async(std::launch::async, [host, port, text, this] { this->asyncStart(host, port, text); });
}

void WebSocket::startSocket(std::string host, std::string text)
{
    startSocket(host, "443", text);
}

// Закрытие сокета
void WebSocket::closeSocket()
{
    closed = true;
    // net::io_context::work

    work.reset();
    ioc.stop();
}

void WebSocket::SendWrite(std::string val)
{
    wr_send = true;
    doc_ = val;
    // work.reset();
    ioc.stop();
}

// Отправка сигнала
void WebSocket::eventData(std::string &data)
{
    if (!closed)
    {
        signalData(data);
    }
}

void WebSocket::eventClose()
{
    signalClose();
}

void WebSocket::eventError(std::string data)
{
    signalError(data);
}

void WebSocket::eventConnect()
{
    signalConnect();
}

// Запуск асинхроного сокета
void WebSocket::asyncStart(std::string host, std::string port, std::string text)
{
    ssl::context ctx(ssl::context::sslv23_client);
    ctx.set_default_verify_paths();

    // Launch the asynchronous operation
    std::shared_ptr link = std::make_shared<BoostWebsock>(ioc, ctx, timeoutGlob);
    // Подключаем сигнал к фукции
    link->signalData = [&, this](std::string dat) { this->eventData(dat); };
    // Подключаем сигнал к фукции
    link->signalClose = [this]() { this->eventClose(); };
    // Подключаем сигнал к фукции
    link->signalError = [&, this](std::string dat) { this->eventError(dat); };

    // Подключаем сигнал к фукции
    link->signalConnect = [this]() { this->eventConnect(); };

    if (wr_send)
    {
        link->run(host, port, text, doc_);
    }
    else
    {
        link->run(host, port, text);
    }

    // Run the I/O service. The call will return when
    // the socket is closed.
    bool first = true;
    bool exit = true;

    // ioc.run();

    while (exit)
    {
        if (!first)
            ioc.restart();
        if (closed)
        {
            ioc.stop();
            // link->closeSock();
            signalClose();
            // ioc.run();
            exit = false;
        }
        else if (first)
        {
            first = false;
            ioc.run();
        }
        else if (wr_send)
        {
            wr_send = false;
            link->send(doc_);
            ioc.run();
        }
        else
        {
            link->runTwoo();
            ioc.run();
        }
    }

    work.reset();

}
