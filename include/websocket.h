#ifndef WEBSOCKET_H
#define WEBSOCKET_H

#include "../private/boostwebsock.h"
#include <iostream>
#include <memory>
#include <string>
#include <chrono>
#include <thread>
#include <future>
#include <queue>


// sing work_guard_type = boost::asio::executor_work_guard<boost::asio::io_context::executor_type>;

class WebSocket
{
public:
    WebSocket(int timeout);
    WebSocket();
    ~WebSocket();

    std::function<void(std::string&)> signalData;
    std::function<void()> signalClose;
    std::function<void(std::string&)> signalError;
    std::function<void()> signalConnect;

    void startSocket(std::string host, std::string port, std::string text);  // Запуск сокета
    void startSocket(std::string host, std::string text);                    // Запуск сокета

    void closeSocket();               // Закрытие сокета
    void SendWrite(std::string val);  // Отправить запись

    std::string idSocket = "Socket";

private:
    net::io_context ioc;
    std::string doc_;
    net::executor_work_guard<decltype(ioc.get_executor())> work{ ioc.get_executor() };

    void asyncStart(std::string host, std::string port, std::string text);  // Запуск асинхроного сокета
    std::string datGlobal;

    std::future<void> q;
    bool closed;
    bool wr_send{false};
    int timeoutGlob;

    void eventData(std::string &data);   // Отправка сигнала
    void eventClose();                  // Закрытие
    void eventError(std::string data);  // Отправка ошибки
    void eventConnect();                // Закрытие
};

#endif