# wsscpp

# C++ Boost Websocket

#### Подключаем библиотеку `CMakeLists.txt`

    add_subdirectory(include/websocket)
    
    ...
    
    # Прописываем в конце
    target_link_libraries(${PROJECT_NAME} PUBLIC lib::websocket_cpp)
    

Создаем WebSocket

	websocket = new WebSocket();

Подключаем события

    // Подключаем сигнал к фукции - Чтение данных
    websocket->Bind(WEBSOCK_RET_DATA, &MainFrame::eventData, this);
    wsocket->signalData = std::bind(&ThisClass::signalData, this, std::placeholders::_1);

    // Подключаем сигнал к фукции - Получение ошибки
    wsocket->signalError = std::bind(&ThisClass::eventError, this, std::placeholders::_1);

    // Подключаем сигнал к фукции - Получение соединения
    wsocket->signalConnect = std::bind(&ThisClass::eventConnect, this);

    // Подключаем сигнал к фукции - Закрыте сокета
    wsocket->signalClose = std::bind(&ThisClass::eventClose, this);


Запускаем сокет (Адрес, Порт, Данные запроса)

    websocket->startSocket("stream.binance.com", "9443", "/ws/btcusdt@kline_5m");

Создаем фукции события

    // Чтение данных
    void MainFrame::eventData(std::string& dataText)
    {
        std::cout << dataText << std::endl;
    }
    
    // Закрыте сокета
    void MainFrame::eventClose()
    {
        std::cout << "Connection closed Wss" << std::endl;
    }
    
    // Получение ошибки
    void MainFrame::eventError(std::string& errorText)
    {
        std::cout << errorText << std::endl;
    }
    
    // Получение соединения
    void MainFrame::eventConnect()
    {
        std::cout << "Connection Wss established" << std::endl;
    }

Вебсокет работает в асинхронном режиме. На каждый сокет отдельный объект.. Если использовать несколько сокетов.. Запускать через вектор объектов
